import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from configurationManager import applicationConfiguration, teamDataConfiguration

class LeaderboardPageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In HomePageHandler::get():\n%s", self.request)

        templateData = templateDataManager.getTemplateData(self.request)

        currentTeamName = ''
        if templateData['teamCode'] in teamDataConfiguration:
            currentTeamName = teamDataConfiguration[templateData['teamCode']]['teamName']
            logging.debug('Current teamName is [%s]' % currentTeamName)

        # Read the individual team score files to construct the leaderboardList
        aggregateScoreData = {}
        leaderboardList = []
        for team in teamDataConfiguration:
            scoreData = teamDataConfiguration[team]['scoreData']
            if scoreData['points'] > 0:
                if scoreData['points'] not in aggregateScoreData:
                    aggregateScoreData[scoreData['points']] = {}

                if scoreData['timeTaken'] not in aggregateScoreData[scoreData['points']]:
                    aggregateScoreData[scoreData['points']][scoreData['timeTaken']] = []

                aggregateScoreData[scoreData['points']][scoreData['timeTaken']].append(teamDataConfiguration[team]['teamName'])

        rank = 1
        for scoreIter in sorted(aggregateScoreData, reverse=True):
            for timeIter in sorted(aggregateScoreData[scoreIter]):
                hrs = int((timeIter) / (60 * 60))
                mins = int((timeIter - (hrs * (60 * 60))) / (60))
                secs = int(timeIter - (hrs * (60 * 60)) - (mins * (60)))
                for teamIter in aggregateScoreData[scoreIter][timeIter]:
                    isThisCurrentTeam = True if currentTeamName is teamIter else False
                    leaderboardList.append([rank, teamIter, scoreIter, ('0' + str(hrs))[-2:], ('0' + str(mins))[-2:], ('0' + str(secs))[-2:], 1 if isThisCurrentTeam else 0])
                rank = rank + 1

        logging.debug('Current leaderboardList:\n%s' % leaderboardList)
        templateData['leaderboard'] = leaderboardList

        # Render the page
        logging.info('Rendering the leaderboard with data about [%s]/[%s]' % (len(templateData['leaderboard']), len(teamDataConfiguration)))
        self.render_response('leaderboardPage.html', **templateData)

