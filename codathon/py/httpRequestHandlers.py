import os
import errno
import inspect
import logging
import webapp2
import mimetypes
from webapp2_extras import jinja2
from jinja2 import Environment, FileSystemLoader

env = Environment(loader = FileSystemLoader(os.path.dirname(inspect.getfile(inspect.currentframe())) + '/../html'))

class BaseRequestHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(app=self.app)

    def render_response(self, _template, **context):
        try:
            # Renders a template and writes the result to the response.
            rv = self.jinja2.render_template(env.get_template(_template), **context)
            logging.debug("Rendering html:\n%s" % rv)
            self.response.write(rv)

        except Exception, e:
            logging.critical('Encountered an error [%s] while trying to render the response for [%s] with the templateData:\n%s' % (e, self.request.path, context))
            self.response.set_status(500)

class StaticDataHandler(BaseRequestHandler):
    def validatePathSafety(self, path):
        if False: #TODO add security check for paths going beyond the root folder here
            raise Exception('SecurityViolation: Request for restricted resource [' + path + '] attempted.')
        return True

    def get(self):
        logging.debug("In StaticDataHandler::get():\n" % self.request)
        try:
            path = os.path.dirname(inspect.getfile(inspect.currentframe())) + '/..' + self.request.path
            self.validatePathSafety(path)

            logging.debug('Trying to open & read file [%s]' % path)

            fh = open(path, 'r')
            self.response.headers.add_header('content-type', mimetypes.guess_type(path)[0])

            try:
                self.response.write(fh.read())

            except Exception, e:
                logging.critical('Encountered an error [%s] while trying to serve resource [%s]' % (e, path))
                
            finally:
                fh.close()  

        except Exception, e:
            logging.critical('Encountered an error [%s] while trying to open & read the resource [%s]' % (e, path))
            self.response.set_status(404 if e.errno is errno.ENOENT else 500)

