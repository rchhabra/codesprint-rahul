# Details regarding all the teams (such as their names, team codes & members) are maintained in this file.
# Each record in the teamData list is a map of the team code to a map of the team's attributes.
# e.g. '7xczyt': { 'teamName': 'team A', 'members': [ 'John Doe', 'Jane Doe', ], },

teamDataConfiguration = {
    '7xczyt':   {
                    'teamName':     'The code wingers',
                    'members':      [ 'John Doe', 'Jane Doe', ],
                },
    'gh9a1v':   {
                    'teamName':     'Code Yodhas',
                    'members':      [ 'Bubble Boy', ],
                },
    'vbzh3v':   {
                    'teamName':     'JIT compilers',
                    'members':      [ 'Stallman', 'Torvalds', 'Drepper', ],
                },
}

